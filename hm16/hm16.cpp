﻿#include <iostream>
#include <string>
#include <stdio.h>
#include <time.h>

int currentDateTime()
{
    time_t     now = time(0);
    struct tm  tstruct;
    tstruct = *localtime(&now);

    int CurrentDay = tstruct.tm_mday;
    return CurrentDay;
}

int main()
{
    const int N = 5;
    int array[N][N];

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j];
        }
        std::cout << std::endl;
    }

    std::cout << "Local Day: " << currentDateTime() << std::endl;

    int Index = currentDateTime() % N;
    std::cout << "Index: " << Index << std::endl;

    int sum = 0;
    for (int j = 0; j < N; j++)
    {
        sum += array[Index][j];
    }

    std::cout << "Sum: " << sum << std::endl;

    return 0;
}

//Final